; main.s
; Desenvolvido para a placa EK-TM4C1294XL
; Prof. Guilherme Peron
; 15/03/2018
; Este programa espera o usu�rio apertar a chave USR_SW2.
; Caso o usu�rio pressione a chave, o LED1 piscar� a cada 1 segundo.

; -------------------------------------------------------------------------------
        THUMB                        ; Instru��es do tipo Thumb-2
; -------------------------------------------------------------------------------
		
; Declara��es EQU - Defines
;<NOME>         EQU <VALOR>
; ========================
; Defini��es de Valores
BIT0	EQU 2_0001
BIT1	EQU 2_0010

; -------------------------------------------------------------------------------
; �rea de Dados - Declara��es de vari�veis
		AREA  DATA, ALIGN=2
		; Se alguma vari�vel for chamada em outro arquivo
		;EXPORT  <var> [DATA,SIZE=<tam>]   ; Permite chamar a vari�vel <var> a 
		                                   ; partir de outro arquivo
;<var>	SPACE <tam>                        ; Declara uma vari�vel de nome <var>
                                           ; de <tam> bytes a partir da primeira 
                                           ; posi��o da RAM		

; -------------------------------------------------------------------------------
; �rea de C�digo - Tudo abaixo da diretiva a seguir ser� armazenado na mem�ria de 
;                  c�digo
        AREA    |.text|, CODE, READONLY, ALIGN=2

		; Se alguma fun��o do arquivo for chamada em outro arquivo	
        EXPORT Start                ; Permite chamar a fun��o Start a partir de 
			                        ; outro arquivo. No caso startup.s
									
		; Se chamar alguma fun��o externa	
        ;IMPORT <func>              ; Permite chamar dentro deste arquivo uma 
									; fun��o <func>
		IMPORT  PLL_Init
		IMPORT  SysTick_Init
		IMPORT  SysTick_Wait1ms			
		IMPORT  GPIO_Init
        IMPORT  PortN_Output
        IMPORT  PortJ_Input
        IMPORT  PortM_Output
        IMPORT  PortL_Input		
		IMPORT  LCD_Init
		IMPORT EnableInterrupts
		IMPORT DisableInterrupts
		;IMPORT Escreve_Numero
		IMPORT LCD_Escreve_Dado
		IMPORT LCD_Escreve_Inst
;		IMPORT PortM_Output_Teclado

		EXPORT Varredura
		EXPORT escreveTecla
; -------------------------------------------------------------------------------
; Fun��o main()
Start  
	PUSH {LR}
	BL PLL_Init                  ;Chama a subrotina para alterar o clock do microcontrolador para 80MHz
	BL SysTick_Init
	BL GPIO_Init                 ;Chama a subrotina que inicializa os GPIO
	BL LCD_Init
	POP {LR}
	BX LR

MainLoop
	MOV R0, #0
    BL Varredura
	CMP R0, #0
	BEQ Pula_Escrita ;Escreve_Numero
	PUSH {R0}
	MOV R1, #0xC0
	BL LCD_Escreve_Inst
	BL SysTick_Wait1ms
	;MOV R1, R0                          ;Espera por 1ms
	POP {R1}
	BL LCD_Escreve_Dado
	MOV R0, #500                             ;Espera por 500ms
	BL SysTick_Wait1ms	
	 
Pula_Escrita
	BL PortJ_Input				 ;Chama a subrotina que l� o estado das chaves e coloca o resultado em R0
Verifica_Nenhuma
	CMP	R0, #2_00000011			 ;Verifica se nenhuma chave est� pressionada
	BNE Verifica_SW2			 ;Se o teste viu que tem pelo menos alguma chave pressionada pula
	MOV R0, #0                   ;N�o acender nenhum LED
	BL PortN_Output			 	 ;Chamar a fun��o para n�o acender nenhum LED
	B MainLoop					 ;Se o teste viu que nenhuma chave est� pressionada, volta para o la�o principal
Verifica_SW2	
	CMP R0, #2_00000001			 ;Verifica se somente a chave SW2 est� pressionada
	BNE MainLoop                 ;Se o teste falhou, volta para o in�cio do la�o principal
	BL Pisca_LED				 ;Chama a rotina para piscar LED
	B MainLoop                   ;Volta para o la�o principal

Varredura
   PUSH {LR}
coluna1   
   MOV R8, #2_00000000
   MOV R0, #2_11100000
   BL PortM_Output;_Teclado
   BL PortL_Input
coluna1linha1
   CMP R0, #2_00001110
   BNE coluna1linha2
   MOV R8, #'1'
   B fimvarredura
coluna1linha2
   CMP R0, #2_00001101
   BNE coluna1linha3
   MOV R8, #'4'
   B fimvarredura   
coluna1linha3
   CMP R0, #2_00001011
   BNE coluna1linha4
   MOV R8, #'7'
   B fimvarredura
coluna1linha4
   CMP R0, #2_00000111
   BNE varre_coluna2
   MOV R8, #'*'
   B fimvarredura   
varre_coluna2   
   MOV R0, #2_11010000
   BL PortM_Output
   BL PortL_Input
coluna2linha1
   CMP R0, #2_00001110
   BNE coluna2linha2
   MOV R8, #'2'
   B fimvarredura
coluna2linha2
   CMP R0, #2_00001101
   BNE coluna2linha3
   MOV R8, #'5'
   B fimvarredura   
coluna2linha3
   CMP R0, #2_00001011
   BNE coluna2linha4
   MOV R8, #'8'
   B fimvarredura
coluna2linha4
   CMP R0, #2_00000111
   BNE varre_coluna3
   MOV R8, #'0'
   B fimvarredura  
varre_coluna3   
   MOV R0, #2_10110000
   BL PortM_Output
   BL PortL_Input
coluna3linha1
   CMP R0, #2_00001110
   BNE coluna3linha2
   MOV R8, #'3'
   B fimvarredura
coluna3linha2
   CMP R0, #2_00001101
   BNE coluna3linha3
   MOV R8, #'6'
   B fimvarredura   
coluna3linha3
   CMP R0, #2_00001011
   BNE coluna3linha4
   MOV R8, #'9'
   B fimvarredura
coluna3linha4
   CMP R0, #2_00000111
   BNE fimvarredura
   MOV R8, #'#'
   ;B fimvarredura  
fimvarredura
	MOV R0,#500				;tratamento de bounce
	BL SysTick_Wait1ms
	
	MOV R0,#0				;APENAS PRA USAR NO .C
   MOV R0, R8
   POP {LR}
   BX LR
   ;POP{PC}



;--------------------------------------------------------------------------------
; Fun��o Pisca_LED
; Par�metro de entrada: N�o tem
; Par�metro de sa�da: N�o tem
Pisca_LED
	MOV R0, #BIT0				 ;Setar o par�metro de entrada da fun��o setando o BIT0
	PUSH {LR}
	BL PortN_Output				 ;Chamar a fun��o para acender o LED1
	MOV R0, #1000                ;Chamar a rotina para esperar 1s
	BL SysTick_Wait1ms
	MOV R0, #0					 ;Setar o par�metro de entrada da fun��o apagando o BIT0
	BL PortN_Output				 ;Chamar a rotina para apagar o LED
	MOV R0, #1000                ;Chamar a rotina para esperar 1s
	BL SysTick_Wait1ms	
	POP {LR}
	BX LR						 ;return
	
;--------------------------------------------------------------------------------
; Coisas que to inventando agora

escreveTecla
	PUSH{LR}
	MOV R1,R0
	BL LCD_Escreve_Dado
	POP{LR}
	BX LR

; -------------------------------------------------------------------------------------------------------------------------
; Fim do Arquivo
; -------------------------------------------------------------------------------------------------------------------------	
    ALIGN                        ;Garante que o fim da se��o est� alinhada 
    END                          ;Fim do arquivo
