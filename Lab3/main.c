// main.c
// Desenvolvido para a placa EK-TM4C1294XL
// Verifica o estado da chave USR_SW2 e acende os LEDs 1 e 2 caso esteja pressionada
// Prof. Guilherme Peron

#include <stdint.h>

#include "tm4c1294ncpdt.h"

void PLL_Init(void);
void LCD_Init(void);
void SysTick_Init(void);
void SysTick_Wait1ms(uint32_t delay);
void SysTick_Wait1us(uint32_t delay);
void GPIO_Init(void);
void Start(void);
void PortA_Output(uint32_t data);
void PortB_Output(uint32_t data);
void PortQ_Output(uint32_t data);
uint32_t PortJ_Input(void);
uint32_t PortL_Input(void);
void PortP_Output(uint32_t data);
void PortK_Output(uint32_t data);
void PortM_Output(uint32_t instr);
//void checkTeclado(void);
void PortH_Output(uint32_t data);
void LCD_Escreve_Dado(uint32_t dado);
uint32_t Varredura(void);
void escreveTecla(uint32_t tecla);
void Clear_Display(void);
void pulaLinha(void);

int motor_A = 1;
int motor_B = 16;
int opcoes_usuario[2][10];
int backupi =0;
int backup =0;


void movMotors(int dir_A,int dir_B)
{
	motor_A = motor_A & 0x0F;
	motor_B = motor_B & 0xF0;
	if(dir_A)
	{
		if(motor_A != 8)
		{
			motor_A = motor_A << 1;
		}
		else
			motor_A = 1;
	}
	else
	{
		if(motor_A != 1)
		{
			motor_A = motor_A >> 1;
		}
		else
			motor_A = 8;
	}
	if(dir_B)
	{
		if(motor_B != 0x80)
		{
			motor_B = motor_B << 1;
		}
		else
			motor_B = 0x10;
	}
	else
	{
		if(motor_B != 0x10)
		{
			motor_B = motor_B >> 1;
		}
		else
			motor_B	 = 0x80;
	}
	PortH_Output(motor_A);
	PortA_Output(motor_B);
}

void zerinho(int time)
{
	while (time >0)
	{
		movMotors(1,1);
		SysTick_Wait1ms(500);
		time --;
	}
}

void turnRight(int t)
{
	for(int time = 0; time <t; time++)
	{
		movMotors(1,1);
		SysTick_Wait1ms(500);
	}
}

void turnLeft(int t)
{
	for(int time = 0; time <t; time++)
	{
		movMotors(0,0);
		SysTick_Wait1ms(500);
	}
}

void goAhead(int time)
{
	while (time >0)
	{
		movMotors(0,1);
		SysTick_Wait1ms(500);
		time --;
	}
}

void goBack (int time)
{
	while (time >0)
	{
		movMotors(1,0);
		SysTick_Wait1ms(500);
		time --;
	}
}

void texto_tecla1() {
		Clear_Display();
		escreveTecla('E');
		escreveTecla('S');
		escreveTecla('C');
		escreveTecla('O');
		escreveTecla('L');
		escreveTecla('H');
		escreveTecla('A');
		escreveTecla(':');
		pulaLinha();
		escreveTecla('1');
		escreveTecla(',');
		escreveTecla(' ');
		escreveTecla('2');
		escreveTecla(',');
		escreveTecla(' ');
		escreveTecla('3');
}

void tecla_1(){
	texto_tecla1();
	while(1){
		
		uint32_t tecla = Varredura();
		if(tecla != 0) {
			Clear_Display();
			escreveTecla(tecla);
			if(tecla == '1'){
				goAhead(1000);
				goBack(1000);
				texto_tecla1();
			}
			else if(tecla == '2'){
				turnRight(1000);
				turnLeft(1000);
				texto_tecla1();
			}
			else if(tecla == '3'){
				turnRight(1000);
				turnRight(1000);
				turnRight(1000);
				turnRight(1000);
				texto_tecla1();
			}
		}
	}
}

void texto_tecla2_etapas() {
		Clear_Display();
		escreveTecla('N');
		escreveTecla('U');
		escreveTecla('M');
		escreveTecla('E');
		escreveTecla('R');
		escreveTecla('O');
		escreveTecla(' ');
		escreveTecla('D');
		escreveTecla('E');
		escreveTecla(' ');
		escreveTecla('E');
		escreveTecla('T');
		escreveTecla('A');
		escreveTecla('P');
		escreveTecla('A');
		escreveTecla('S');
		pulaLinha();
}

void texto_tecla2_movimento(int i){
		char n = (char)i + 48;
		escreveTecla('M');
		escreveTecla('O');
		escreveTecla('V');
		escreveTecla(' ');
		escreveTecla(n);
		//escreveTecla('D');
		//escreveTecla('O');
		//escreveTecla(' ');
		//escreveTecla('C');
		//escreveTecla('A');
		//escreveTecla('R');
		//escreveTecla('R');
		//escreveTecla('I');
		//escreveTecla('N');
		//escreveTecla('H');
		//escreveTecla('O');
		pulaLinha();
}

void texto_tecla2_tempo(){
		escreveTecla('T');
		escreveTecla('E');
		escreveTecla('M');
		escreveTecla('P');
		escreveTecla('O');
		escreveTecla(' ');
		escreveTecla('D');
		escreveTecla('O');
		escreveTecla(' ');
		escreveTecla('M');
		escreveTecla('O');
		escreveTecla('V');
		escreveTecla('M');
		escreveTecla('N');
		escreveTecla('T');
		escreveTecla('O');
		pulaLinha();
}

void Executar(){
	for(int i = 0; i<10;i++)
	{
		switch(opcoes_usuario[1][i])
		{
			case 2:
				goAhead(opcoes_usuario[0][i]*1000);
			break;
			case 4:
				turnLeft(opcoes_usuario[0][i]*1000);
			break;
			case 6:
				turnRight(opcoes_usuario[0][i]*1000);
			break;
			case 8:
				goBack(opcoes_usuario[0][i]*1000);
			break;
			case 0:
			break;
		}
	}
}

void tecla_2(){
	int tecla = 0;
	texto_tecla2_etapas();
	// vari�veis usadas para armazenar as op��es escolhidas pelo usu�rio
	// velocidade, sentido de cada motor e o tempo de execu��o de cada etapa
	// vetor 0 = tempo, 1 = sentido dos motores (usar o teclado como joystick)
	for(int i = 0; i<10 ; i++){
		for(int j=0 ; j<10 ; j++){
			opcoes_usuario[i][j] = 0;
		}
	}
		SysTick_Wait1ms(100000);
		while(tecla == 0)
		{
			tecla = Varredura();
		}
		// pegou o numero de etapas
		int numero_etapas = (int)tecla - 48;
		escreveTecla(tecla);
		SysTick_Wait1ms(100000);
		// numero de etapas selecionadas pelo usu�rio
		for(int i =0; i < numero_etapas;i++){
				backupi = i;
				backup = numero_etapas;
				Clear_Display();
				i = backupi;
				texto_tecla2_movimento(backupi);
				tecla = 0;
				while(tecla == 0){
					tecla = Varredura();
				}
				escreveTecla(tecla);
				opcoes_usuario[1][backupi] = (int)tecla - 48;
				SysTick_Wait1ms(100000);
				Clear_Display();
				i = backupi;
				tecla = 0;
				texto_tecla2_tempo();
				while(tecla == 0){
					tecla = Varredura();
				}
				escreveTecla(tecla);
				opcoes_usuario[0][backupi] = (int)tecla - 48;
				SysTick_Wait1ms(100000);	
				numero_etapas = backup;
				i = backupi;
		}
		SysTick_Wait1ms(30000);
		Executar();

}

int main(void)
{
	Start();
	//zerinho(2000);
	//turnRight();
	//turnLeft();
	//goAhead(1000);
	//goBack(500);
	while(1)
	{
		uint32_t tecla = Varredura();
		if(tecla != 0) {
			Clear_Display();
			escreveTecla(tecla);
			if(tecla == '1'){
				tecla_1();
			}
			if(tecla == '2'){
				tecla_2();
			}
		}
		SysTick_Wait1ms(500);
		
		
	}
}


