; lcd.s
; Desenvolvido para a placa EK-TM4C1294XL
; Prof. Guilherme Peron
; RS = PM0
; R/W = PM1
; EN  = PM2

; -------------------------------------------------------------------------------
        THUMB                        ; Instru��es do tipo Thumb-2
; -------------------------------------------------------------------------------
; Declara��es EQU - Defines
; ========================
; Defini��es de Valores
	
	

; -------------------------------------------------------------------------------
; �rea de C�digo - Tudo abaixo da diretiva a seguir ser� armazenado na mem�ria de 
;                  c�digo
        AREA    |.text|, CODE, READONLY, ALIGN=2

		IMPORT PortM_Output
		IMPORT PortK_Output
		IMPORT SysTick_Wait1us
		IMPORT SysTick_Wait1ms
			
		; Se alguma fun��o do arquivo for chamada em outro arquivo	
        EXPORT LCD_Init             ; Permite chamar LCD_Init de outro arquivo
		EXPORT LCD_Escreve_Dado		
		EXPORT LCD_Escreve_Inst
		EXPORT Clear_Display
		EXPORT pulaLinha
;--------------------------------------------------------------------------------
; Fun��o GPIO_Init
; Par�metro de entrada: N�o tem
; Par�metro de sa�da: N�o tem
LCD_Init
;=====================
			PUSH {LR}
			; Espera por 15ms antes de fazer qualquer coisa
			MOV R1, #2_000                 ;Par�metro de entrada da fun��o LCD_Command
			BL LCD_Command				  ;Chama a fun��o escreve comando no LCD
			MOV R0, #15                   ;Par�metro de entrada da fun��o SysTick_Wait1ms
			BL SysTick_Wait1ms            ;Espera 1ms por 15 vezes
			
			MOV R1, #0x38                 ;2 linhas / matriz 7x5
			BL LCD_Escreve_Inst	
			MOV R1, #0x38                 ;2 linhas / matriz 7x5
			BL LCD_Escreve_Inst
			MOV R1, #0x06
			BL LCD_Escreve_Inst
			MOV R1, #0x0E
			BL LCD_Escreve_Inst
			MOV R1, #0x01
			BL LCD_Escreve_Inst
			MOV R0, #1600
			BL SysTick_Wait1us			
			
			MOV R1, #'1'
			BL LCD_Escreve_Dado
			MOV R1, #':'
			BL LCD_Escreve_Dado
			MOV R1, #'P'
			BL LCD_Escreve_Dado
			MOV R1, #'R'
			BL LCD_Escreve_Dado	
			MOV R1, #'E'
			BL LCD_Escreve_Dado	
			MOV R1, #'-'
			BL LCD_Escreve_Dado	
			MOV R1, #'P'
			BL LCD_Escreve_Dado	
			MOV R1, #'R'
			BL LCD_Escreve_Dado	
			MOV R1, #'O'
			BL LCD_Escreve_Dado	
			MOV R1, #'G'
			BL LCD_Escreve_Dado	
			MOV R1, #'R'
			BL LCD_Escreve_Dado	
			MOV R1, #'A'
			BL LCD_Escreve_Dado	
			MOV R1, #'M'
			BL LCD_Escreve_Dado	
			MOV R1, #'A'
			BL LCD_Escreve_Dado	
			MOV R1, #'D'
			BL LCD_Escreve_Dado	
			MOV R1, #'O'
			BL LCD_Escreve_Dado	
			
			MOV R1, #0xC0			;pula linha
			BL LCD_Escreve_Inst
			BL SysTick_Wait1ms
			
			MOV R1, #'2'
			BL LCD_Escreve_Dado
			MOV R1, #':'
			BL LCD_Escreve_Dado
			MOV R1, #'C'
			BL LCD_Escreve_Dado
			MOV R1, #'U'
			BL LCD_Escreve_Dado	
			MOV R1, #'S'
			BL LCD_Escreve_Dado	
			MOV R1, #'T'
			BL LCD_Escreve_Dado	
			MOV R1, #'O'
			BL LCD_Escreve_Dado	
			MOV R1, #'M'
			BL LCD_Escreve_Dado	
			MOV R1, #'I'
			BL LCD_Escreve_Dado	
			MOV R1, #'Z'
			BL LCD_Escreve_Dado	
			MOV R1, #'A'
			BL LCD_Escreve_Dado	
			MOV R1, #'D'
			BL LCD_Escreve_Dado	
			MOV R1, #'O'
			BL LCD_Escreve_Dado	

			
;			LDR R2, =UTFPR
;loop_str	LDRB R1, [R2],#1
;			CBZ R1, fim_loop_str
;			BL LCD_Escreve_Dado
;			B loop_str
			
;fim_loop_str 
			POP {LR}
			BX LR



; -------------------------------------------------------------------------------
; Fun��o LCD_Command
; Par�metro de entrada: R1 --> os bits RS, R/W e EN
; Par�metro de sa�da: N�o tem
LCD_Command
	PUSH {LR}       						;Link Register
	MOV R0, R1
	BL PortM_Output
	POP {LR}
	BX LR
	;POP {PC}								;Retorno


; -------------------------------------------------------------------------------
; Fun��o LCD_Data
; Par�metro de entrada: R1 --> o valor dos dados para o LCD
; Par�metro de sa�da: N�o tem
LCD_Data
	PUSH {LR}       						;Link Register
	MOV R0, R1
	BL PortK_Output
	POP {LR}
	BX LR
	;POP {PC}								;Retorno
	
; -------------------------------------------------------------------------------
; Fun��o LCD_Escreve_Inst
; Par�metro de entrada: R1 --> Comando em hexadecimal
; Par�metro de sa�da: N�o tem
LCD_Escreve_Inst
	PUSH {LR}       						;Link Register
	BL LCD_Data                             ;Coloca no barramento de dados o comando
	MOV R1, #2_000                          ;E=0, R/W=0, RS=0
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #6                              ;Espera por 6us
	BL SysTick_Wait1us
	
	MOV R1, #2_100                          ;E=1, R/W=0, RS=0
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #6                              ;Espera por 6us
	BL SysTick_Wait1us	
	
	MOV R1, #2_000                          ;E=0, R/W=0, RS=0
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #40                             ;Espera por 40us
	BL SysTick_Wait1us	
	POP {LR}
	BX LR
	;POP {PC}								;Retorno

; -------------------------------------------------------------------------------
; Fun��o LCD_Escreve_Dado
; Par�metro de entrada: R1 --> Dado em hexadecimal
; Par�metro de sa�da: N�o tem
LCD_Escreve_Dado
	PUSH {LR}       						;Link Register
	BL LCD_Data                             ;Coloca no barramento de dados o comando
	MOV R1, #2_000                          ;E=0, R/W=0, RS=X (tanto faz)
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #6                              ;Espera por 6us
	BL SysTick_Wait1us
	
	MOV R1, #2_101                          ;E=1, R/W=0, RS=1
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #6000                              ;Espera por 6us
	BL SysTick_Wait1us	
	
	MOV R1, #2_000                          ;E=0, R/W=0, RS=X (tanto faz)
	BL LCD_Command                          ;Envia o comando para o LCD
	MOV R0, #40                             ;Espera por 40us
	BL SysTick_Wait1us

	MOV R0, #1600
	BL SysTick_Wait1us
	
	POP {LR}
	BX LR
	;POP {PC}								;Retorno	

UTFPR DCB "UTFPR - MICRO",0 

; -------------------------------------------------------------------------------
; COISAS QUE EU INVENTEI


Clear_Display
	PUSH{LR}
	MOV R5,#0x01
	BL Port_K_Write
	MOV R6,#2_100
	BL Port_M_Write
	;desabilitando o display
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#2
	BL SysTick_Wait1ms
	;fim
	POP {LR}
	BX LR
	
Port_M_Write
	PUSH{LR}
	MOV R0,R6
	BL PortM_Output
	;MOV R0,#2
	;BL SysTick_Wait1ms
	POP{LR}
	BX LR

Port_K_Write
	PUSH{LR}
	MOV R0,R5
	BL PortK_Output
	MOV R0,#6
	BL SysTick_Wait1us
	POP{LR}
	BX LR
	
pulaLinha
	PUSH{LR}
	MOV R1, #0xC0			;pula linha
	BL LCD_Escreve_Inst
	BL SysTick_Wait1ms
	POP{LR}
	BX LR


	

    ALIGN                           ; garante que o fim da se��o est� alinhada 
    END                             ; fim do arquivo