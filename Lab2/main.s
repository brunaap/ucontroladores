; main.s
; Desenvolvido para a placa EK-TM4C1294XL
; Prof. Guilherme Peron
; Ver 1 19/03/2018
; Ver 2 26/08/2018
; Este programa deve esperar o usu�rio pressionar uma chave.
; Caso o usu�rio pressione uma chave, um LED deve piscar a cada 1 segundo.

; -------------------------------------------------------------------------------
        THUMB                        ; Instru��es do tipo Thumb-2
; -------------------------------------------------------------------------------
		
; Declara��es EQU - Defines
;<NOME>         EQU <VALOR>
; ========================
; Defini��es de Valores


; -------------------------------------------------------------------------------
; �rea de Dados - Declara��es de vari�veis
		AREA  DATA, ALIGN=2
		; Se alguma vari�vel for chamada em outro arquivo
		;EXPORT  <var> [DATA,SIZE=<tam>]   ; Permite chamar a vari�vel <var> a 
		                                   ; partir de outro arquivo
;<var>	SPACE <tam>                        ; Declara uma vari�vel de nome <var>
                                           ; de <tam> bytes a partir da primeira 
                                           ; posi��o da RAM		
Numero_Mult SPACE 10

; -------------------------------------------------------------------------------
; �rea de C�digo - Tudo abaixo da diretiva a seguir ser� armazenado na mem�ria de 
;                  c�digo
        AREA    |.text|, CODE, READONLY, ALIGN=2

		; Se alguma fun��o do arquivo for chamada em outro arquivo	
        EXPORT Start                ; Permite chamar a fun��o Start a partir de 
			                        ; outro arquivo. No caso startup.s
									
		; Se chamar alguma fun��o externa	
        ;IMPORT <func>              ; Permite chamar dentro deste arquivo uma 
									; fun��o <func>
		IMPORT  PLL_Init
		IMPORT  SysTick_Init
		IMPORT  SysTick_Wait1ms		
		IMPORT  SysTick_Wait1us			
		IMPORT  GPIO_Init
        IMPORT  PortA_Output
		IMPORT  PortB_Output
		IMPORT  PortQ_Output
        IMPORT  PortJ_Input
		IMPORT 	PortL_Input
		IMPORT  PortP_Output
		IMPORT  PortK_Output
		IMPORT  PortM_Output

		IMPORT 	Leds


; -------------------------------------------------------------------------------
; Fun��o main()
Start  		
	BL PLL_Init                  ;Chama a subrotina para alterar o clock do microcontrolador para 80MHz
	BL SysTick_Init              ;Chama a subrotina para inicializar o SysTick
	BL GPIO_Init                 ;Chama a subrotina que inicializa os GPIO

	MOV R6,#2_00000000
	
	LDR R4,=Numero_Mult	;passando o vetor para R4
	MOV R0,#0
	MOV R1,#0
	MOV R3,R4
	
while
	ADD R3,R1	;iterando o vetor
	STR R0,[R3]	;zerando o que est� no endere�o armazenado por R4
	
	ADD R1,#1	;iterando o loop
	CMP R1,#10
	IT NE
		BNE while
		
	
	
	

	BL Initialize
	BL SetCursor
	BL Clear_Display
	MOV R0,#250
	BL SysTick_Wait1ms
	
	MOV R5,#'S'
	BL Instruction
	MOV R5,#'l'
	BL Instruction
	MOV R5,#'i'
	BL Instruction
	MOV R5,#'d'
	BL Instruction
	MOV R5,#'e'
	BL Instruction
	MOV R5,#' '
	BL Instruction
	MOV R5,#'t'
	BL Instruction
	MOV R5,#'a'
	BL Instruction
	MOV R5,#' '
	BL Instruction
	MOV R5,#'e'
	BL Instruction
	MOV R5,#'r'
	BL Instruction
	MOV R5,#'r'
	BL Instruction
	MOV R5,#'a'
	BL Instruction
	MOV R5,#'d'
	BL Instruction
	MOV R5,#'o'
	BL Instruction
	MOV R5,#'!'
	BL Instruction
	
Loop
	BL checkTeclado
	B Loop



;TECLADO



checkTeclado
	PUSH {LR}
;verificando primeira coluna
	MOV R0,#2_11100000
	BL PortM_Output
	BL PortL_Input
	CMP R0,#2_00001111 ;R0 representa as linhas, par acomparar com as linhas
	IT EQ ;se estiver igual nenhum bot�o foi pressionado
		BEQ coluna2 ;se estiver igual nenhum bot�o foi pressionado, proxima coluna
	CMP R0,#2_00001110 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#1
		BEQ saida
	CMP R0,#2_00001101 ;segunda linha pressionada
	ITT EQ
		MOVEQ R10,#4
		BEQ saida
	CMP R0,#2_00001011 ;terceira linha pressionada
	ITT EQ
		MOVEQ R10,#7
		BEQ saida
	CMP R0,#2_00000111 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#'*'
		BEQ saida
;verificando segunda coluna
coluna2
	MOV R0,#2_11010000
	BL PortM_Output
	BL PortL_Input
	CMP R0,#2_00001111 ;R0 representa as linhas, par acomparar com as linhas
	IT EQ ;se estiver igual nenhum bot�o foi pressionado
		BEQ coluna3 ;se estiver igual nenhum bot�o foi pressionado, proxima coluna
	CMP R0,#2_00001110 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#2
		BEQ saida
	CMP R0,#2_00001101 ;segunda linha pressionada
	ITT EQ
		MOVEQ R10,#5
		BEQ saida
	CMP R0,#2_00001011 ;terceira linha pressionada
	ITT EQ
		MOVEQ R10,#8
		BEQ saida
	CMP R0,#2_00000111 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#0
		BEQ saida
	POP {LR}
	BX LR
;verificando terceira coluna
coluna3
	MOV R0,#2_10110000
	BL PortM_Output
	BL PortL_Input
	CMP R0,#2_00001111 ;R0 representa as linhas, par acomparar com as linhas
	IT EQ ;se estiver igual nenhum bot�o foi pressionado
		BEQ saida ;se estiver igual nenhum bot�o foi pressionado, proxima coluna
	CMP R0,#2_00001110 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#3
		BEQ saida
	CMP R0,#2_00001101 ;segunda linha pressionada
	ITT EQ
		MOVEQ R10,#6
		BEQ saida
	CMP R0,#2_00001011 ;terceira linha pressionada
	ITT EQ
		MOVEQ R10,#9
		BEQ saida
	CMP R0,#2_00000111 ;primeira linha pressionada
	ITT EQ
		MOVEQ R10,#'#'
		BEQ saida
saida

	MOV R0,#500	;tratamento de bounce
	BL SysTick_Wait1ms

	BL PortL_Input ;caso n�o tenha nada em R2 - nenhum bot�o pressionado
	CMP R0,#2_00001111
	IT EQ
		BEQ vaza
		
	BL Clear_Display
	MOV R0,#250
	BL SysTick_Wait1ms
	
	BL Armazenar_dados
	
	;se n�o vazou, significa que um n�mero foi presionado
	MOV R5,#'T'
	BL Instruction
	MOV R5,#'a'
	BL Instruction
	MOV R5,#'b'
	BL Instruction
	MOV R5,#'u'
	BL Instruction
	MOV R5,#'a'
	BL Instruction
	MOV R5,#'d'
	BL Instruction
	MOV R5,#'a'
	BL Instruction
	MOV R5,#' '
	BL Instruction
	MOV R5,#'d'
	BL Instruction
	MOV R5,#'o'
	BL Instruction
	MOV R5,#' '
	BL Instruction
	ADD R5,R10,#48
	BL Instruction
	;sinal de multiplica��o
	MOV R5,#0XC0
	BL SetCursor
	ADD R5,R10,#48
	BL Instruction
	MOV R5,#'X'
	BL Instruction
	;numero de vezes que apertou o bot�o
	ADD R5,R8,#48
	BL Instruction
	;sinal de igual
	MOV R5,#'='
	BL Instruction
	
	;resultado da multiplica��o
	MOV R0,#10 ;para imprimir numeros maiores que 9
	MUL R5,R10,R8 
	;dezena
	CMP R5,#10 ;verificar se o valor a ser impresso n�o � maior que 9
	ITT CS ;se for menor que 10 imprime n�mero -> dezena
		UDIVCS R9,R5,R0
		MOVCS R5,R9 ;para encontrarmos a unidade
	ADD R5,#48
	BL Instruction
	;unidade
	MOV R0,#10
	MUL R5,R10,R8
	MUL R9,R9,R0 ;multiplica o numero da dezena por 10 
	CMP R5,#10
	ITTT CS ;se for menor que 10 imprime n�mero -> unidade
		SUBCS R5,R9 ;subtraindo do numero para extrair a unidade
		ADDCS R5,#48
		BLCS Instruction ;aqui s� pode imprimir algo se diver dezena e unidade
	
	
	;delay para imprimir apenas um numero por vez que aperta o bot�o
	MOV R0,#50000
	BL SysTick_Wait1ms
	
vaza

	POP {LR}
	BX LR
	

; MULTIPLICA

Armazenar_dados
	PUSH{LR}
	MOV R0,R4
	ADD R0,R10
	LDRB R8,[R0]
	ADD R8,#1
	CMP R8,#10
	IT EQ
		MOVEQ R8,#0
	
	STRB R8,[R0]
		
	POP{LR}
	BX LR

;DISPLAY LCD




Initialize
	PUSH{LR}
	;inicializar no modo duas linhas
	MOV R6,#0x00
	BL Port_M_Write
	MOV R5,#0x38
	BL Port_K_Write
	MOV R6,#2_100
	BL Port_M_Write
	;desabilitando o display
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#40
	BL SysTick_Wait1us
	;fim
	;cursor com auto incremento para a direita
	MOV R5,#0x06														
	BL Port_K_Write
	MOV R6,#2_100
	BL Port_M_Write
	;desabilitando o display
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#40
	BL SysTick_Wait1us
	;fim
	POP {LR}
	BX LR	
	
SetCursor
	PUSH{LR}
	BL Port_K_Write
	MOV R6,#2_100
	BL Port_M_Write
	;desabilitando o display
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#40
	BL SysTick_Wait1us
	;fim
	POP {LR}
	BX LR

Clear_Display
	PUSH{LR}
	MOV R5,#0x01
	BL Port_K_Write
	MOV R6,#2_100
	BL Port_M_Write
	;desabilitando o display
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#2
	BL SysTick_Wait1ms
	;fim
	POP {LR}
	BX LR

Port_M_Write
	PUSH{LR}
	MOV R0,R6
	BL PortM_Output
	;MOV R0,#2
	;BL SysTick_Wait1ms
	POP{LR}
	BX LR

Port_K_Write
	PUSH{LR}
	MOV R0,R5
	BL PortK_Output
	MOV R0,#6
	BL SysTick_Wait1us
	POP{LR}
	BX LR
	
PosicionaCursor
	PUSH{LR}
	MOV R5,R7
	BL Port_K_Write
	MOV R6,#2_100
	
	BL Port_M_Write
	MOV R0,#10
	BL SysTick_Wait1us
	;desabilitando o LCD
	MOV R6,#0x00
	BL Port_M_Write
	MOV R0,#40
	BL SysTick_Wait1us
	;fim
	POP{LR}
	BX LR
	
Instruction
	PUSH{LR}
	;colocando o cursor na posi��o
	;MOV R7,#0x80
	;BL PosicionaCursor
	;enviando o dado
	BL Port_K_Write
	;desabilitando o display
	MOV R6,#2_000
	BL Port_M_Write
	MOV R0,#6
	BL SysTick_Wait1us
	;fim
	MOV R6,#2_101
	BL Port_M_Write
	MOV R0,#10
	BL SysTick_Wait1us
	;desabilitando o display
	MOV R6,#2_001
	BL Port_M_Write
	MOV R0,#40
	BL SysTick_Wait1us
	;fim
	POP{LR}
	BX LR


	
    ALIGN                        ;Garante que o fim da se��o est� alinhada 
    END                          ;Fim do arquivo

