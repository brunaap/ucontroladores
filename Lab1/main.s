; main.s
; Desenvolvido para a placa EK-TM4C1294XL
; Prof. Guilherme Peron
; Ver 1 19/03/2018
; Ver 2 26/08/2018
; Este programa deve esperar o usu�rio pressionar uma chave.
; Caso o usu�rio pressione uma chave, um LED deve piscar a cada 1 segundo.

; -------------------------------------------------------------------------------
        THUMB                        ; Instru��es do tipo Thumb-2
; -------------------------------------------------------------------------------
		
; Declara��es EQU - Defines
;<NOME>         EQU <VALOR>
; ========================
; Defini��es de Valores


; -------------------------------------------------------------------------------
; �rea de Dados - Declara��es de vari�veis
		AREA  DATA, ALIGN=2
		; Se alguma vari�vel for chamada em outro arquivo
		;EXPORT  <var> [DATA,SIZE=<tam>]   ; Permite chamar a vari�vel <var> a 
		                                   ; partir de outro arquivo
;<var>	SPACE <tam>                        ; Declara uma vari�vel de nome <var>
                                           ; de <tam> bytes a partir da primeira 
                                           ; posi��o da RAM		

; -------------------------------------------------------------------------------
; �rea de C�digo - Tudo abaixo da diretiva a seguir ser� armazenado na mem�ria de 
;                  c�digo
        AREA    |.text|, CODE, READONLY, ALIGN=2

		; Se alguma fun��o do arquivo for chamada em outro arquivo	
        EXPORT Start                ; Permite chamar a fun��o Start a partir de 
			                        ; outro arquivo. No caso startup.s
									
		; Se chamar alguma fun��o externa	
        ;IMPORT <func>              ; Permite chamar dentro deste arquivo uma 
									; fun��o <func>
		IMPORT  PLL_Init
		IMPORT  SysTick_Init
		IMPORT  SysTick_Wait1ms			
		IMPORT  GPIO_Init
        IMPORT  PortA_Output
		IMPORT  PortB_Output
		IMPORT  PortQ_Output
        IMPORT  PortJ_Input
		IMPORT  PortP_Output
		IMPORT 	Leds


; -------------------------------------------------------------------------------
; Fun��o main()
Start  		
	BL PLL_Init                  ;Chama a subrotina para alterar o clock do microcontrolador para 80MHz
	BL SysTick_Init              ;Chama a subrotina para inicializar o SysTick
	BL GPIO_Init                 ;Chama a subrotina que inicializa os GPIO

Variaveis
	MOV R7,#0					;Contador do display da direita
	MOV R9,#0					;Contador do display da esquerda
	MOV R6,#0					;Seleciona o transistor
	MOV R10,#0					;Constante que roda um loop para deixar os registradores acesos
	MOV R8,#0					;Define quando se o display ser� incrementado ou n�o
	MOV R11,#1					;Velocidade da contagem
	MOV R3,#0					;Contador dos LEDS
	
MainLoop
	
	BL Leds

	BL PortJ_Input				 ;Chama a subrotina que l� o estado das chaves e coloca o resultado em R0
	CMP R0,#2
	IT EQ
		SUBEQ R11,#1
	CMP R0,#1
	IT EQ
		ADDEQ R11,#1
	
	
	CMP R11,#9					;Controlador da velocidade da contagem
	IT CS
		MOVCS R11,#0
	CMP R11,#0
	IT CC
		MOVCC R11,#9
		
	CMP R11,#0
	IT EQ
		ADDEQ R8,#1					;Controla a velocidade dos Displays
	CMP R11,#1
	IT EQ
		ADDEQ R8,#2					;Controla a velocidade dos Displays
	CMP R11,#2
	IT EQ
		ADDEQ R8,#5					;Controla a velocidade dos Displays
	CMP R11,#3
	IT EQ
		ADDEQ R8,#7					;Controla a velocidade dos Displays
	CMP R11,#4
	IT EQ
		ADDEQ R8,#10					;Controla a velocidade dos Displays
	CMP R11,#5
	IT EQ
		ADDEQ R8,#12					;Controla a velocidade dos Displays
	CMP R11,#6
	IT EQ
		ADDEQ R8,#15					;Controla a velocidade dos Displays
	CMP R11,#7
	IT EQ
		ADDEQ R8,#17					;Controla a velocidade dos Displays
	CMP R11,#8
	IT EQ
		ADDEQ R8,#20					;Controla a velocidade dos Displays
	CMP R11,#9
	IT EQ
		ADDEQ R8,#25					;Controla a velocidade dos Displays
		
		
	CMP R8,#1000
	ITTT CS 
		ADDCS R7,#1
		MOVCS R8,#0
		ADDCS R3,#1
		
	CMP R3,#14
	IT EQ
		MOVEQ R3,#0
		
	ADD R6,#1
	
	CMP R7,#10					;Se o da direita for maior ou igual a 10 volta pra 0
	ITT EQ
		MOVEQ R7,#0
		ADDEQ R9,#1
		
	CMP R9,#10					;Se o da esquerda for maior ou igual a 10 volta pra 0
	IT EQ
		MOVEQ R9,#0
		
	CMP R6,#2					;Se for maior ou igual a 2 volta pra 0
	IT EQ
		MOVEQ R6,#0
	
	CMP R6,#0
	ITE EQ
		MOVEQ R5,R9
		MOVNE R5,R7
		
	
	B Switch
	
	
	;ADD R10,#1
	;CMP R10,#1000				;Loop interno para deixar os displays acesos
	;BCC MainLoop
	
	B MainLoop
	
Switch
	B Display
Numeros
	CMP R5,#0
	BEQ Numero_0
	CMP R5,#1
	BEQ Numero_1
	CMP R5,#2
	BEQ Numero_2
	CMP R5,#3
	BEQ Numero_3
	CMP R5,#4
	BEQ Numero_4
	CMP R5,#5
	BEQ Numero_5
	CMP R5,#6
	BEQ Numero_6
	CMP R5,#7
	BEQ Numero_7
	CMP R5,#8
	BEQ Numero_8
	CMP R5,#9
	BEQ Numero_9
	
Display
	CMP R6,#0
	BEQ Display_1
	CMP R6,#1
	BEQ Display_2

Display_1
	MOV R0, #2_00010000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortB_Output				 ;Chamar a fun��o para setar o LED3
	B Numeros

Display_2
	MOV R0, #2_00100000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortB_Output				 ;Chamar a fun��o para setar o LED3
	B Numeros
	



Numero_0
	;MOV R0, #2_00110000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	MOV R0, #0x30
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	;MOV R0, #2_00001111			 ;Setar o par�metro de entrada da fun��o como o BIT4
	MOV R0, #0x0F
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop

Numero_1
	MOV R0, #2_00000000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00000110			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_2
	MOV R0, #2_01010000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00001011			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop

Numero_3
	MOV R0, #2_01000000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00001111			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_4
	MOV R0, #2_01100000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00000110			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_5
	MOV R0, #2_01100000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00001101			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_6
	MOV R0, #2_01110000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00001101			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_7
	MOV R0, #2_00000000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00000111			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop

Numero_8
	MOV R0, #2_01110000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00001111			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop
	
Numero_9
	MOV R0, #2_01100000			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortA_Output				 ;Chamar a fun��o para setar o LED3
	MOV R0, #2_00000111			 ;Setar o par�metro de entrada da fun��o como o BIT4
	BL PortQ_Output				 ;Chamar a fun��o para setar o LED3
	BL SysTick_Wait1ms
	B MainLoop

    ALIGN                        ;Garante que o fim da se��o est� alinhada 
    END                          ;Fim do arquivo

